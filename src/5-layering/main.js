var TaskItem = React.createClass({
    render: function () {
        return (
            <tr key={this.props.taskIndex}>
                <td>{this.props.taskIndex + 1}</td>
                <td>{this.props.task.text}</td>
                <td>
                    <span className={this.props.task.done ? "glyphicon glyphicon-check pointer"
                                                          : "glyphicon glyphicon-unchecked pointer"}
                          aria-hidden="true" value={this.props.taskIndex}
                          onClick={this.props.toogleDone}></span>
                </td>
                <td>
                    <span className="glyphicon glyphicon-remove-sign pointer"
                          aria-hidden="true" value={this.props.taskIndex}
                          onClick={this.props.deleteTask}></span>
                </td>
            </tr>
        );
    }
});

var TaskList = React.createClass({
    render: function () {
        return (
            <table className="table table-striped">
                <thead>
                <tr>
                    <th className="col-md-1">#</th>
                    <th className="col-md-9">Task</th>
                    <th className="col-md-1">Done</th>
                    <th className="col-md-1">Remove</th>
                </tr>
                </thead>
                <tbody>
                {this.props.items.map((task, taskIndex) => {
                    return (
                        <TaskItem task={task}
                                  taskIndex={taskIndex}
                                  deleteTask={this.props.deleteTask}
                                  toogleDone={this.props.toogleDone}
                                  key={taskIndex}/>
                  );
                })}
                </tbody>
            </table>
        );
    }
});

var TaskApp = React.createClass({
    render: function () {
        return (
            <div>
                <h1>Tasks</h1>
                <TaskList items={this.state.items}
                          deleteTask={this.deleteTask}
                          toogleDone={this.toogleDone}/>
                <form className="form-inline pull-right" onSubmit={this.addTask}>
                    <div className="form-group">
                        <input type="text" className="form-control"
                               id="newTask" placeholder="New task"
                               onChange={this.onChange}
                               value={this.state.newTask}/>
                    </div>
                    <button type="submit" className="btn btn-default">Add Task</button>
                </form>
            </div>
        );
    },

    getInitialState: function () {
        return {
            items: [
                {text: "Tread the thrones of the Earth under my feet", done: false},
                {text: "Win ten straight victories in Mortal Kombat", done: true},
                {text: "Terminate the command of Colonel Kurtz", done: true}
            ],
            newTask: ''
        }
    },

    onChange: function (e) {
        this.setState({newTask: e.target.value});
    },

    toogleDone: function (e) {
        var taskIndex = _.toInteger(e.target.value);
        this.state.items[taskIndex].done = !this.state.items[taskIndex].done;
        this.forceUpdate(); // Forces a re-render of this element.
        e.preventDefault();
    },

    deleteTask: function (e) {
        var taskIndex = _.toInteger(e.target.value);
        this.setState(state => {
            state.items.splice(taskIndex, 1);
            return {items: state.items};
        });
        e.preventDefault();
    },

    addTask: function (e) {
        this.setState({
            items: this.state.items.concat([{text: this.state.newTask, done: false}]),
            newTask: ''
        });
        e.preventDefault();
    }
});

ReactDOM.render(<TaskApp />, document.getElementById('content'));