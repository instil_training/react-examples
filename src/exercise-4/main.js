// Can filter via a text input on the top of the page

const api = restful('http://localhost:8080');
const flightsCollection = api.all('flights');

var FlightBookingApp = React.createClass({
    getInitialState: function () {
        return {flights: [], orderBy: {}}
    },

    componentDidMount: function () {
        this.loadFlightsFromServer();
    },

    loadFlightsFromServer: function() {
        flightsCollection.getAll().then((response) => {
            const flights = response.body().map((flightEntity) => {
                return flightEntity.data();
            });

            this.setState({flights: flights});
        });
    },

    updateFlightsState: function(flights, orderBy) {
        if (flights === undefined) {
            flights = this.state.flights;
        }

        if (orderBy !== undefined) {
            var reOrderedFlights = _.orderBy(flights, orderBy.propertyName, orderBy.type);
            this.setState({orderBy: orderBy});
        } else {
            var reOrderedFlights = _.orderBy(flights, this.state.orderBy.propertyName, this.state.orderBy.type);
        }

        this.setState({flights: reOrderedFlights});
    },

    render: function () {
        return (
            <div>
                <FlightsFilter flights={this.state.flights} updateFlightsState={this.updateFlightsState}/>
                <FlightBookingTable flights={this.state.flights} updateFlightsState={this.updateFlightsState}/>
            </div>
        );
    }
});

var FlightsFilter = React.createClass({
    getInitialState: function () {
        return {unFilteredFlights: [], filter: ''}
    },

    componentWillReceiveProps: function() {
        if (this.state.unFilteredFlights.length === 0) {
            this.setState({
                unFilteredFlights: this.props.flights
            });
        }
    },

    render: function() {
        return (
            <input type="text" className="form-control pull-left" id="filter" placeholder="Filter" 
                   onChange={this.applyFilter} value={this.state.filter}/>
       );
    },

    applyFilter: function(event) {
        var filter = event.target.value;
        if (filter === '') {
            this.props.updateFlightsState(this.state.unFilteredFlights);
        } else {
            var filteredFlights = _.filter(this.props.flights, (flight) => {
                return ('' + flight.number).includes(filter) ||
                       flight.origin.includes(filter) ||
                       flight.destination.includes(filter);
            });
            this.props.updateFlightsState(filteredFlights);
        }

        this.setState({
            filter: filter
        });
    }
});

var FlightBookingTable = React.createClass({
    render: function() {
        return (
            <table className="table table-striped">
                <FlightBookingTableHeader flights={this.props.flights} updateFlightsState={this.props.updateFlightsState}/>
                <FlightBookingTableBody flights={this.props.flights} />
            </table>
        )
    }
});

var FlightBookingTableHeader = React.createClass({
    getInitialState: function() {
        return {orderBy: {}};
    },

    render: function() {
        return (
            <thead>
                <tr>
                    <th className="pointer col-md-1" onClick={() => this.filterBy('number')}>
                        #<OrderByIcon propertyName={'number'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="pointer col-md-2" onClick={() => this.filterBy('origin')}>
                        Origin<OrderByIcon propertyName={'origin'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="pointer col-md-2" onClick={() => this.filterBy('departure')}>
                        Departure Time<OrderByIcon propertyName={'departure'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="pointer col-md-2" onClick={() => this.filterBy('destination')}>
                        Destination<OrderByIcon propertyName={'destination'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="pointer col-md-2" onClick={() => this.filterBy('arrival')}>
                        Arrival Time<OrderByIcon propertyName={'arrival'} orderBy={this.state.orderBy}/>
                    </th>
                </tr>
            </thead>
        );
    },

    filterBy: function(propertyName) {
        var newOrderBy = this.getNewSelectedOrderBy(propertyName);
        this.setState({orderBy: newOrderBy});

        this.props.updateFlightsState(undefined, newOrderBy);
    },

    getNewSelectedOrderBy: function(propertyName) {
        if (this.state.orderBy.propertyName === propertyName) {
            return {
                propertyName: propertyName,
                type: this.state.orderBy.type === 'desc' ? 'asc' : 'desc'
            };
        } else {
            return {
                propertyName: propertyName,
                type: 'asc'
            };
        }
    }
});

var OrderByIcon = React.createClass({
    render: function() {
        return <span className={"" + this.addIcon()} />
    },

    addIcon: function() {
        if (this.props.orderBy.propertyName === this.props.propertyName) {
            return 'glyphicon glyphicon-chevron-' + (this.props.orderBy.type === 'desc' ? 'up' : 'down');
        }
        return '';
    }
});

var FlightBookingTableBody = React.createClass({
    render: function() {
        return (
            <tbody>
                {this.props.flights.map((flight, index) => {
                    return (
                        <tr key={index}>
                            <td>{flight.number}</td>
                            <td>{flight.origin}</td>
                            <td>{new Date(flight.departure).toLocaleString()}</td>
                            <td>{flight.destination}</td>
                            <td>{new Date(flight.arrival).toLocaleString()}</td>
                        </tr>
                    )
                })}
            </tbody>
        )
    }
});

ReactDOM.render(<FlightBookingApp />, document.getElementById('content'));