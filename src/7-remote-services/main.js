const api = restful('http://localhost:8080');
const coursesCollection = api.all('courses');

var CourseApp = React.createClass({
    getInitialState: function () {
        return {
            courses: [],
            selectedCourse: {},
            modifiedCourse: {}
        };
    },

    componentDidMount: function () {
        this.loadCoursesFromServer();
    },

    deleteCourse: function (e) {
        coursesCollection.delete(e.target.value).then((data) => {
            this.setState({selectedCourse: {}});
            this.setState({modifiedCourse: {}});
            this.loadCoursesFromServer()
        });
        e.preventDefault();
    },

    editCourse: function (e) {
        coursesCollection.get(e.target.value).then((data) => {
            this.setState({selectedCourse: {}});
            this.setState({modifiedCourse: data.body().data()});
        });
        e.preventDefault();
    },

    viewCourse: function (e) {
        coursesCollection.get(e.target.value).then((data) => {
            this.setState({modifiedCourse: {}});
            this.setState({selectedCourse: data.body().data()});
        });
        e.preventDefault();
    },

    updateCourse: function (e) {
        coursesCollection.put(this.state.modifiedCourse.id, this.state.modifiedCourse).then(() => {
            this.loadCoursesFromServer();
            this.setState({modifiedCourse: {}});
        });
        e.preventDefault();
    },

    loadCoursesFromServer: function () {
        coursesCollection.getAll().then((response) => {
            const courses = response.body().map((courseEntity) => {
                return courseEntity.data();
            });
            this.setState({courses: courses});
        });
    },

    render: function () {
        return (
            <div>
                <h1>Courses</h1>
                <CourseList courses={this.state.courses} viewCourse={this.viewCourse} deleteCourse={this.deleteCourse}
                            editCourse={this.editCourse}/>
                <ViewCourse selectedCourse={this.state.selectedCourse}/>
                <EditCourse modifiedCourse={this.state.modifiedCourse} updateCourse={this.updateCourse}/>
            </div>
        );
    }
});

var CourseList = React.createClass({
    render: function () {
        var rows = this.props.courses.map((course, courseIndex) =>
                <CourseRow key={courseIndex} course={course} viewCourse={this.props.viewCourse}
                           deleteCourse={this.props.deleteCourse}
                           editCourse={this.props.editCourse}/>
        );

        return (
            <table className="table table-striped">
                <thead>
                <tr>
                    <th className="col-md-1">#</th>
                    <th className="col-md-8">Title</th>
                    <th className="col-md-1"></th>
                    <th className="col-md-1"></th>
                    <th className="col-md-1"></th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </table>
        );
    }
});

var CourseRow = React.createClass({
    render: function () {
        var course = this.props.course;
        return (
            <tr key={this.props.courseIndex}>
                <td>{course.id}</td>
                <td>{course.title}</td>
                <ButtonCell buttonStyle="info" action={this.props.viewCourse} id={course.id} text="View"/>
                <ButtonCell buttonStyle="warning" action={this.props.editCourse} id={course.id} text="Edit"/>
                <ButtonCell buttonStyle="danger" action={this.props.deleteCourse} id={course.id} text="Delete"/>
            </tr>
        );
    }
});

var ButtonCell = React.createClass({
    render: function () {
        return (
            <td>
                <button type="button" className={"btn btn-" + this.props.buttonStyle} aria-label="Center Align"
                        onClick={this.props.action} value={this.props.id}>
                    <span className="glyphicon glyphicon-eye-open" aria-hidden="true"></span> {this.props.text}
                </button>
            </td>
        );
    }
});

var ViewCourse = React.createClass({
    renderTable: function(course) {
        if(!_.isEmpty(course)) {
            return (
                <table className="table table-condensed table-striped">
                    <tbody>
                        <tr>
                            <td><strong>ID</strong></td>
                            <td>{course.id}</td>
                        </tr>
                        <tr>
                            <td><strong>Title</strong></td>
                            <td>{course.title}</td>
                        </tr>
                        <tr>
                            <td><strong>Difficulty</strong></td>
                            <td>{course.difficulty}</td>
                        </tr>
                        <tr>
                            <td><strong>Duration</strong></td>
                            <td>{course.duration}</td>
                        </tr>
                    </tbody>
                </table>
            );
        }
    },
    
    render: function () {
        var selectedCourse = this.props.selectedCourse;
        return (<div>{this.renderTable(selectedCourse)}</div>);
    }
});

var EditCourse = React.createClass({
    updateSelectedCourseState: function (key, value) {
        var updatedCourseState = this.props.modifiedCourse;
        updatedCourseState[key] = value;
        this.setState({modifiedCourse: updatedCourseState});
        this.forceUpdate();
    },

    renderForm: function(course) {
        if(!_.isEmpty(course)) {
            return(
                <div>
                    <h2>Update course</h2>
                    <form onSubmit={this.props.updateCourse}>
                        <div className="form-group">
                            <label for="courseId">Course ID</label>
                            <input id="courseId" className="form-control" type="text" size="10"
                                   value={course.id || ''}
                                   onChange={(e) => this.updateSelectedCourseState('id', e.target.value)}/>
                        </div>
                        <div className="form-group">
                                <label for="courseTitle">Course Title:</label>
                                <input id="courseTitle" className="form-control" type="text"
                                    value={course.title || ''}
                                    onChange={(e) => this.updateSelectedCourseState('title', e.target.value)}/>
                        </div>

                        <div className="form-group">
                            <label for="courseType">Course Type:</label>
                            <select id="courseType" className="form-control"
                                    value={course.difficulty || ''}
                                    onChange={(e) => this.updateSelectedCourseState('difficulty', e.target.value)}>
                                <option value="BEGINNER">Beginner</option>
                                <option value="INTERMEDIATE">Intermediate</option>
                                <option value="ADVANCED">Advanced</option>
                            </select>
                        </div>

                        <div className="form-group">
                            <label for="duration">Length of Course:</label>
                            <input id="duration" className="form-control" type="number" min="1" max="5"
                                   value={course.duration || ''}
                                   onChange={(e) => this.updateSelectedCourseState('duration', e.target.value)}/>
                        </div>

                        <input type="submit" value="Update Course"/>
                    </form>
                </div>
            );
        }
    },

    render: function () {
        return (
            <div>{this.renderForm(this.props.modifiedCourse)}</div>
        );
    }
});

ReactDOM.render(<CourseApp />, document.getElementById('content'));
