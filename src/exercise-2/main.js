// The flights now appear in a table generated with react

const api = restful('http://localhost:8080');
const flightsCollection = api.all('flights');

var FlightBookingApp = React.createClass({
    getInitialState: function () {
        return {flights: []}
    },

    componentDidMount: function () {
        this.loadFlightsFromServer();
    },

    loadFlightsFromServer: function() {
        flightsCollection.getAll().then((response) => {
            const flights = response.body().map((flightEntity) => {
                return flightEntity.data();
            });

            this.setState({flights: flights});
        });
    },

    render: function () {
        return (
            <FlightBookingTable flights={this.state.flights}/>
        );
    }
});

var FlightBookingTable = React.createClass({
    render: function() {
        return (
            <table className="table table-striped">
                <FlightBookingTableHeader />
                <FlightBookingTableBody flights={this.props.flights} />
            </table>
        )
    }
});

var FlightBookingTableHeader = React.createClass({
    render: function() {
        return (
            <thead>
                <tr>
                    <th className="col-md-1">#</th>
                    <th className="col-md-2">Origin</th>
                    <th className="col-md-2">Departure Time</th>
                    <th className="col-md-2">Destination</th>
                    <th className="col-md-2">Arrival Time</th>
                </tr>
            </thead>
        );
    }
});

var FlightBookingTableBody = React.createClass({
    render: function() {
        return (
            <tbody>
                {this.props.flights.map((flight, index) => {
                    return (
                        <tr key={index}>
                            <td>{flight.number}</td>
                            <td>{flight.origin}</td>
                            <td>{new Date(flight.departure).toLocaleString()}</td>
                            <td>{flight.destination}</td>
                            <td>{new Date(flight.arrival).toLocaleString()}</td>
                        </tr>
                    )
                })}
            </tbody>
        )
    }
});

ReactDOM.render(<FlightBookingApp />, document.getElementById('content'));