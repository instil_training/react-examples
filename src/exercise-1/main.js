// The same as the previous exercise, except now using react instead of just vanilla javascript

const api = restful('http://localhost:8080'); // Set our base api url
const flightsCollection = api.all('flights'); // Set our service url

var FlightBookingApp = React.createClass({
    getInitialState: function () { // Set the inital state of our component
        return {flights: []}
    },

    componentDidMount: function () { // Called after the component has been added to the dom
        this.loadFlightsFromServer();
    },

    loadFlightsFromServer: function() {
        flightsCollection.getAll().then((response) => { // Get an array from the server
            const flights = response.body().map((flightEntity) => { // Retrieve the data from each response element
                return flightEntity.data();
            });
            this.setState({
                flights: js_beautify(JSON.stringify(flights)) // Convert json object to a string and then pretty print
            });
        });
    },

    render: function () {
        return (
            <pre>
            	{this.state.flights}
            </pre>
        );
    }
});

ReactDOM.render(<FlightBookingApp />, document.getElementById('content'));