function xhrEventHandler() {
    function addFlightsToDocument(responseText) {
        var textAsNiceJson = js_beautify(responseText);
        var containerElement = document.createElement('pre');

        containerElement.textContent = textAsNiceJson;
        document.getElementById('content')
                .appendChild(containerElement);
    }
    // Ensure our ready state is finished (4) and we get a HTTP ok (200)
    if (this.readyState === 4 && this.status === 200) {
        addFlightsToDocument(this.responseText);
    }
}

// A vanilla ajax call to retrieve the flights from the server
var httpRequest = new XMLHttpRequest();
httpRequest.onreadystatechange = xhrEventHandler;
httpRequest.open('GET', 'http://localhost:8080/flights');
httpRequest.send(); 