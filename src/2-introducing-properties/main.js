var HelloMessage = React.createClass({
    render: function() { // Contains the JSX to be rendered to the screen.
    	return <h1>Hello, {this.props.name}!</h1>;
    }
});

ReactDOM.render(
    <HelloMessage name="Fred" />, // Call our reactive component instead of rendering the JSX directly as in the previous example.
    document.getElementById('content')
);