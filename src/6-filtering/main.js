var TaskRow = React.createClass({
    render: function () {
        return (
            <tr key={this.props.taskIndex}>
                <td>{this.props.taskIndex + 1}</td>
                <td>{this.props.task.text}</td>
                <td>
                    <span className={this.props.task.done ? "glyphicon glyphicon-check pointer" : "glyphicon glyphicon-unchecked pointer"}
                          aria-hidden="true" 
                          value={this.props.taskIndex}
                          onClick={this.props.toogleDone}>
                    </span>
                </td>
                <td>
                    <span className="glyphicon glyphicon-remove-sign pointer" 
                          aria-hidden="true"
                          value={this.props.taskIndex}
                          onClick={this.props.deleteTask}></span>
                </td>
            </tr>
        );
    }

});

var TaskList = React.createClass({
    render: function () {
        var filter = this.props.filter;
        var tasks = this.props.items.filter(task => !filter || _.toLower(task.text).indexOf(_.toLower(filter)) != -1);

        var rows = tasks.map((task, taskIndex) =>
            <TaskRow key={taskIndex}
                     taskIndex={taskIndex}
                     task={task}
                     toogleDone={this.props.toogleDone}
                     deleteTask={this.props.deleteTask}/>
        );


        return (
            <table className="table table-striped">
                <thead>
                <tr>
                    <th className="col-md-1">#</th>
                    <th className="col-md-9">Task</th>
                    <th className="col-md-1">Done</th>
                    <th className="col-md-1">Remove</th>
                </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        );
    }
});

var TaskApp = React.createClass({
    getInitialState: function () {
        return {
            items: [
                {text: "Learn JS", done: false}, 
                {text: "Learn React", done: true}
            ],
            newTask: '',
            filter: ''
        }
    },

    onChange: function (e) {
        this.setState({newTask: e.target.value});
    },

    updateFilter: function (e) {
        this.setState({filter: e.target.value});
        this.forceUpdate();
        e.preventDefault();
    },

    toogleDone: function (e) {
        var taskIndex = _.toInteger(e.target.value);
        this.state.items[taskIndex].done = !this.state.items[taskIndex].done;
        this.forceUpdate();
        e.preventDefault();
    },

    deleteTask: function (e) {
        var taskIndex = _.toInteger(e.target.value);
        this.setState(state => {
            state.items.splice(taskIndex, 1);
            return {items: state.items};
        });
        e.preventDefault();
    },

    addTask: function (e) {
        this.setState({
            items: this.state.items.concat([{text: this.state.newTask, done: false}]),
            newTask: ''
        });
        e.preventDefault();
    },

    render: function () {
        return (
            <div>
                <h1>Tasks</h1>
                <input type="text" className="form-control pull-left" id="filter" placeholder="Filter" 
                       onChange={this.updateFilter} value={this.state.filter}/>
                <TaskList items={this.state.items} deleteTask={this.deleteTask} toogleDone={this.toogleDone} 
                          filter={this.state.filter}/>
                <form className="form-inline pull-right" onSubmit={this.addTask}>
                    <div className="form-group">
                        <input type="text" className="form-control" id="newTask" placeholder="New task" 
                               onChange={this.onChange} value={this.state.newTask}/>
                    </div>
                    <button type="submit" className="btn btn-default">Add Task</button>
                </form>
            </div>
        );
    }
});

ReactDOM.render(<TaskApp />, document.getElementById('content'));