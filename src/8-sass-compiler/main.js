var codeExamples = {
  nesting: (
    "nav {\n" +
    "  ul {\n" +
    "    margin: 0;\n" +
    "    padding: 0;\n" +
    "    list-style: none;\n" +
    "  }\n" +
    "\n" +
    "  li { display: inline-block; }\n" +
    "\n" +
    "  a {\n" +
    "    display: block;\n" +
    "    padding: 6px 12px;\n" +
    "    text-decoration: none;\n" +
    "  }\n" +
    "}"
  ),
  variables: (
    "$my-background-color: black;\n" +
    "$my-border-style: 3px red dotted;\n" +
    "$main-text-color: red;\n" +
    "$sub-text-color: gray;\n" +
    "\n" +
    ".my-widget {\n" +
    "  background: $my-background-color;\n" +
    "  border: $my-border-style;\n" +
    "}\n" +
    "\n" +
    ".my-navbar {\n" +
    "  background: $my-background-color;\n" +
    "  border: $my-border-style;\n" +
    "}\n" +
    "\n" +
    ".my-wonderful-text {\n" +
    "  color: $main-text-color;\n" +
    "}\n" +
    "\n" +
    ".my-bio-text {\n" +
    "  color: $main-text-color;\n" +
    "}\n" +
    "\n" +
    ".my-copyright {\n" +
    "  color: $sub-text-color;\n" +
    "}\n"
  ),
  functions: (
    "$my-primary-brand-color: #FF00FF;\n" +
    "$my-secondary-brand-color: #00FF00;\n" +
    "$grim-dark-primary-brand-color: darken($my-primary-brand-color, 30%);\n" +
    "$grim-dark-brand-color: darken($my-secondary-brand-color, 30%);\n" +
    "$enable-grim-dark-styles: true;\n" +
    "\n" +
    ".navbar {\n" +
    "  background: if($enable-grim-dark-styles, $grim-dark-primary-brand-color, $my-primary-brand-color);\n" +
    "  border-color: if($enable-grim-dark-styles, $grim-dark-brand-color, $my-secondary-brand-color);\n" +
    "}\n" +
    "\n" +
    ".widget {\n" +
    "  background: if($enable-grim-dark-styles, $grim-dark-primary-brand-color, $my-primary-brand-color);\n" +
    "  border-color: if($enable-grim-dark-styles, $grim-dark-brand-color, $my-secondary-brand-color);\n" +
    "}\n" +
    "\n" +
    ".content {\n" +
    "  color: if($enable-grim-dark-styles, $grim-dark-primary-brand-color, $my-primary-brand-color);\n" +
    "}\n"
  ),
  extending: (
    "$favourite-color: red;\n" +
    "\n" +
    ".my-prefered-colors {\n" +
    "  color: $favourite-color;\n" +
    "  background-color: $favourite-color;\n" +
    "  border-color: $favourite-color;\n" +
    "}\n" +
    "\n" +
    "%my-awesome-placeholder-style {\n" +
    "  @extend .my-prefered-colors;\n" +
    "  font-size: 72px;\n" +
    "  border: 10px solid;\n" +
    "  border-radius: 5px;\n" +
    "}\n" +
    "\n" +
    ".my-widget {\n" +
    "  @extend %my-awesome-placeholder-style;\n" +
    "  border-width: 1px;\n" +
    "}"
  ),
  mixin: (
    "@mixin border-radius($radius) {\n" +
    "  -webkit-border-radius: $radius;\n" +
    "     -moz-border-radius: $radius;\n" +
    "      -ms-border-radius: $radius;\n" +
    "          border-radius: $radius;\n" +
    "}\n" +
    "\n" +
    ".box { @include border-radius(10px); }"
  ),
  calculations: (
    ".container { width: 100%; }\n" +
    "\n" +
    'article[role="main"] {\n' +
    "  float: left;\n" +
    "  width: 600px / 960px * 100%;\n" +
    "}\n" +
    "\n" +
    'aside[role="complementary"] {\n' +
    "  float: right;\n" +
    "  width: 300px / 960px * 100%;\n" +
    "}"
  )
}

var SassCompiler = React.createClass({
  getInitialState: function() {
    return {
      scss: codeExamples.nesting,
      compiledScss: ''
    }
  },

  render: function() {
    return (
      <div>
        <h3>Examples</h3>
        <select className="form-control" onChange={this.codeExampleSelected} defaultValue="nesting">
          <option value="nesting">Nesting</option>
          <option value="variables">Variables</option>
          <option value="functions">Functions</option>
          <option value="extending">Extending</option>
          <option value="mixin">Mixin</option>
          <option value="calculations">Calculations</option>
        </select>

        <h3>SCSS to compile <span className={"text-danger" + (this.state.hasError ? '' : ' hidden')}>{this.state.errorText}</span></h3>
        <textarea className="form-control" onChange={this.scssManuallyChanged} value={this.state.scss} rows="15"/>

        <h3>Compiled SCSS</h3>
          <div className={this.state.compiledScss === '' ? 'hidden' : ''}>
          <pre>
              {this.state.compiledScss}
          </pre>
        </div>
      </div>
    );
  },

  codeExampleSelected: function(event) {
    this.updateScss(codeExamples[event.target.value]);
  },

  scssManuallyChanged: function(event) {
    var scss = event.target.value;
    this.setState({
      scss: scss,
      compiledScss: this.state.compiledScss
    })
    this.updateScss(scss);
    event.preventDefault();
  },

  componentDidMount: function() {
    this.updateScss(this.state.scss);
  },

  updateScss: function(scss) {
    var self = this;
    Sass.compile(scss, function(compiledScss) {
      if (compiledScss.text !== undefined) {
        self.setState({
          scss: scss,
          compiledScss: css_beautify(compiledScss.text),
          hasError: false
        });
        self.forceUpdate();
      } else {
        self.setState({
          scss: scss,
          errorText: 'Error at line ' + compiledScss.line + ':' + compiledScss.column + '. Reason: ' + compiledScss.message,
          hasError: true
        });
      }
    });
  }
});

ReactDOM.render(
    <SassCompiler />,
    document.getElementById('content')
);