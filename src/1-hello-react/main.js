ReactDOM.render(
    // This is not HTMl but JSX - the reason we need to label react files as a JSX or Babel scripts.
    <h1>Hello, world!</h1>,

    // We could use javascript but its not as easy to read or as intuitive. e.g. 
    //React.createElement('h1', 'Hello, world!')

    document.getElementById('content') // The DOM element we will render on.
);