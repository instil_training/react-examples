const api = restful('http://localhost:8080');
const flightsCollection = api.all('flights');

var FlightBookingApp = React.createClass({
    getInitialState: function () {
        return {flights: [], orderBy: {
            propertyName: 'number',
            type: 'asc'
        }}
    },

    componentDidMount: function () {
        this.loadFlightsFromServer();
    },

    loadFlightsFromServer: function() {
        flightsCollection.getAll().then((response) => {
            const flights = response.body().map((flightEntity) => {
                return flightEntity.data();
            });

            this.updateFlightsState(flights);
        });
    },

    updateFlightsState: function(flights, orderBy) {
        if (flights === undefined) {
            flights = this.state.flights;
        }

        if (orderBy !== undefined) {
            var reOrderedFlights = _.orderBy(flights, orderBy.propertyName, orderBy.type);
            this.setState({orderBy: orderBy});
        } else {
            var reOrderedFlights = _.orderBy(flights, this.state.orderBy.propertyName, this.state.orderBy.type);
        }

        this.setState({flights: reOrderedFlights});
    },

    updateFlight: function(flight) {
        flightsCollection.put(flight.number, flight).then(() => {
            this.loadFlightsFromServer();
        });
    },

    deleteFlight: function(flightNumber) {
        flightsCollection.delete(flightNumber).then((data) => {
            this.loadFlightsFromServer()
        });
    },

    render: function () {
        return (
            <div>
                <FlightsFilter flights={this.state.flights} 
                               updateFlightsState={this.updateFlightsState}/>
                <FlightBookingTable flights={this.state.flights} 
                                    orderBy={this.state.orderBy}
                                    updateFlightsState={this.updateFlightsState} 
                                    updateFlight={this.updateFlight} 
                                    deleteFlight={this.deleteFlight}/>
            </div>
        );
    }
});

var FlightsFilter = React.createClass({
    getInitialState: function () {
        return {unFilteredFlights: [], filter: ''}
    },

    componentWillReceiveProps: function() {
        if (this.state.unFilteredFlights.length === 0) {
            this.setState({
                unFilteredFlights: this.props.flights
            });
        }
    },

    render: function() {
        return (
            <input type="text" className="form-control pull-left" id="filter" placeholder="Filter" 
                   onChange={this.applyFilter} value={this.state.filter}/>
       );
    },

    applyFilter: function(event) {
        var filter = event.target.value;
        if (filter === '') {
            this.props.updateFlightsState(this.state.unFilteredFlights);
        } else {
            var filteredFlights = _.filter(this.state.unFilteredFlights, (flight) => {
                return ('' + flight.number).includes(filter) ||
                       flight.origin.includes(filter) ||
                       flight.destination.includes(filter);
            });
            this.props.updateFlightsState(filteredFlights);
        }

        this.setState({
            filter: filter
        });
    }
});

var FlightBookingTable = React.createClass({
    render: function() {
        return (
            <table className="table table-striped">
                <FlightBookingTableHeader flights={this.props.flights} 
                                          orderBy={this.props.orderBy}
                                          updateFlightsState={this.props.updateFlightsState}/>
                <FlightBookingTableBody flights={this.props.flights} 
                                        updateFlight={this.props.updateFlight} 
                                        deleteFlight={this.props.deleteFlight}/>
            </table>
        )
    }
});

var FlightBookingTableHeader = React.createClass({
    getInitialState: function() {
        return {orderBy: this.props.orderBy};
    },

    render: function() {
        return (
            <thead>
                <tr>
                    <th className="pointer col-md-1" onClick={() => this.filterBy('number')}>
                        #<OrderByIcon propertyName={'number'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="pointer col-md-2" onClick={() => this.filterBy('origin')}>
                        Origin<OrderByIcon propertyName={'origin'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="pointer col-md-2" onClick={() => this.filterBy('departure')}>
                        Departure Time<OrderByIcon propertyName={'departure'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="pointer col-md-2" onClick={() => this.filterBy('destination')}>
                        Destination<OrderByIcon propertyName={'destination'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="pointer col-md-2" onClick={() => this.filterBy('arrival')}>
                        Arrival Time<OrderByIcon propertyName={'arrival'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="col-md-3"></th>
                </tr>
            </thead>
        );
    },

    filterBy: function(propertyName) {
        var newOrderBy = this.getNewOrderBy(propertyName);
        this.setState({orderBy: newOrderBy});

        this.props.updateFlightsState(undefined, newOrderBy);
    },

    getNewOrderBy: function(propertyName) {
        if (this.state.orderBy.propertyName === propertyName) {
            return {
                propertyName: propertyName,
                type: this.state.orderBy.type === 'desc' ? 'asc' : 'desc'
            };
        } else {
            return {
                propertyName: propertyName,
                type: 'asc'
            };
        }
    }
});

var OrderByIcon = React.createClass({
    render: function() {
        return <span className={"" + this.addIcon()} />
    },

    addIcon: function() {
        if (this.props.orderBy.propertyName === this.props.propertyName) {
            return 'glyphicon glyphicon-chevron-' + (this.props.orderBy.type === 'desc' ? 'up' : 'down');
        }
        return '';
    }
});

var FlightBookingTableBody = React.createClass({
    render: function() {
        return (
            <tbody>
                {this.props.flights.map((flight, index) => {
                    return (
                        <FlightBookingTableRow flight={flight} 
                                               index={index} 
                                               updateFlight={this.props.updateFlight} 
                                               deleteFlight={this.props.deleteFlight}/>
                    )
                })}
            </tbody>
        )
    }
});

var FlightBookingTableRow = React.createClass({
    getInitialState: function() {
        return {updatedFlight: this.props.flight, isEditing: false};
    },

    render: function() {
        if (!this.state.isEditing) {
            return (
                <tr key={this.props.index}>
                    <td>
                        {this.props.flight.number}
                    </td>
                    <td>
                        {this.props.flight.origin}
                    </td>
                    <td>
                        {new Date(this.props.flight.departure).toLocaleString()}
                    </td>
                    <td>
                        {this.props.flight.destination}
                    </td>
                    <td>
                        {new Date(this.props.flight.arrival).toLocaleString()}
                    </td>
                    <td>
                        <div className="btn-toolbar">
                            <button className={"btn btn-small btn-default" + (this.state.isEditing ? ' hidden' : '')} 
                                    type="button" 
                                    onClick={this.onEdit}>
                                Edit
                            </button>
                            <button className={"btn btn-small btn-danger"} 
                                    type="button"
                                    onClick={this.onDelete}>
                                Delete
                            </button>
                        </div>
                    </td>
                </tr>
            );
        } else {
            return (
                <tr key={this.props.index}>
                    <td>
                        <input type="text" value={this.state.updatedFlight.number} onChange={(e) => this.updateProperty(e, 'number')}/>
                    </td>
                    <td>
                        <input type="text" value={this.state.updatedFlight.origin} onChange={(e) => this.updateProperty(e, 'origin')}/>
                    </td>
                    <td>
                        <input type="date" value={this.state.updatedFlight.departure} onChange={(e) => this.updateProperty(e, 'departure')}/>
                    </td>
                    <td>
                        <input type="text" value={this.state.updatedFlight.destination} onChange={(e) => this.updateProperty(e, 'destination')}/>
                    </td>
                    <td>
                        <input type="text" value={this.state.updatedFlight.arrival} onChange={(e) => this.updateProperty(e, 'arrival')}/>
                    </td>
                    <td>
                        <div className="btn-toolbar">
                            <button className={"btn btn-small btn-success" + (this.state.isEditing ? '' : ' hidden')} 
                                    type="button"
                                    onClick={this.onSave}>
                                Save
                            </button>
                            <button className={"btn btn-small btn-danger"} 
                                    type="button"
                                    onClick={this.onDelete}>
                                Delete
                            </button>
                        </div>
                    </td>
                </tr>
            );
        }
    },

    updateProperty: function(event, propertyName) {
        var updatedFlight = this.state.updatedFlight;
        updatedFlight[propertyName] = event.target.value;
        this.setState({
            updatedFlight: updatedFlight
        });
    },

    onEdit: function() {
        this.setState({
            isEditing: true
        });
    },

    onSave: function() {
        this.setState({
            isEditing: false
        });
        this.props.updateFlight(this.props.flight);
    },

    onDelete: function() {
        this.props.deleteFlight(this.props.flight.number);
    }
});

ReactDOM.render(<FlightBookingApp />, document.getElementById('content'));