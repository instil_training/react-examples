var MyWidget = React.createClass({
    getInitialState: function () {
        return {text: this.props.startText};
    },
    onChange: function (e) {
        this.setState({text: e.target.value});
    },
    render: function () {
        var date = new Date().toLocaleTimeString();
        var sectionStyle = {border: 'thin solid black',
                            width: '50%', padding: '1em'};
        return (
            <section style={sectionStyle}>
                <h4>Type to update me:</h4>
                <input type="text"
                       onChange={this.onChange}
                       value={this.state.text}/>
                <div><i>Rendered at {date}</i></div>
            </section>
        );
    }
});
ReactDOM.render(
    <div>
        <MyWidget startText="foo"/>
        <hr/>
        <MyWidget startText="wibble"/>
    </div>,
    document.getElementById('content')
);

