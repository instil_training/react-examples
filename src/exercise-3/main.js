// Table headers will set the order by of each column

const api = restful('http://localhost:8080');
const flightsCollection = api.all('flights');

var FlightBookingApp = React.createClass({
    getInitialState: function () {
        return {flights: []}
    },

    componentDidMount: function () {
        this.loadFlightsFromServer();
    },

    loadFlightsFromServer: function() {
        flightsCollection.getAll().then((response) => {
            const flights = response.body().map((flightEntity) => {
                return flightEntity.data();
            });

            this.setState({flights: flights});
        });
    },

    updateFlightsState: function(flights) {
        this.setState({flights: flights});
    },

    render: function () {
        return (
            <FlightBookingTable flights={this.state.flights}
                                updateFlightsState={this.updateFlightsState}/>
        );
    }
});

var FlightBookingTable = React.createClass({
    render: function() {
        return (
            <table className="table table-striped">
                <FlightBookingTableHeader flights={this.props.flights}
                                          updateFlightsState={this.props.updateFlightsState}/>
                <FlightBookingTableBody flights={this.props.flights} />
            </table>
        )
    }
});

var FlightBookingTableHeader = React.createClass({
    getInitialState: function() {
        return {orderBy: {}};
    },

    render: function() {
        return (
            <thead>
                <tr>
                    <th className="pointer col-md-1" onClick={() => this.orderFlightsBy('number')}>
                        #<OrderByIcon propertyName={'number'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="pointer col-md-2" onClick={() => this.orderFlightsBy('origin')}>
                        Origin<OrderByIcon propertyName={'origin'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="pointer col-md-2" onClick={() => this.orderFlightsBy('departure')}>
                        Departure Time<OrderByIcon propertyName={'departure'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="pointer col-md-2" onClick={() => this.orderFlightsBy('destination')}>
                        Destination<OrderByIcon propertyName={'destination'} orderBy={this.state.orderBy}/>
                    </th>
                    <th className="pointer col-md-2" onClick={() => this.orderFlightsBy('arrival')}>
                        Arrival Time<OrderByIcon propertyName={'arrival'} orderBy={this.state.orderBy}/>
                    </th>
                </tr>
            </thead>
        );
    },

    orderFlightsBy: function(propertyName) {
        var newOrderBy = this.getNewSelectedOrderBy(propertyName);
        this.setState({orderBy: newOrderBy});

        var reOrderedFlights = _.orderBy(this.props.flights, newOrderBy.propertyName, newOrderBy.type);
        this.props.updateFlightsState(reOrderedFlights);
    },

    getNewSelectedOrderBy: function(propertyName) {
        if (this.state.orderBy.propertyName === propertyName) {
            return {
                propertyName: propertyName,
                type: this.state.orderBy.type === 'desc' ? 'asc' : 'desc'
            };
        } else {
            return {
                propertyName: propertyName,
                type: 'asc'
            };
        }
    }
});

var OrderByIcon = React.createClass({
    render: function() {
        return <span className={"" + this.addIcon()} />
    },

    addIcon: function() {
        if (this.props.orderBy.propertyName === this.props.propertyName) {
            return 'glyphicon glyphicon-chevron-' + (this.props.orderBy.type === 'desc' ? 'up' : 'down');
        }
        return '';
    }
});

var FlightBookingTableBody = React.createClass({
    render: function() {
        return (
            <tbody>
                {this.props.flights.map((flight, index) => {
                    return (
                        <tr key={index}>
                            <td>{flight.number}</td>
                            <td>{flight.origin}</td>
                            <td>{new Date(flight.departure).toLocaleString()}</td>
                            <td>{flight.destination}</td>
                            <td>{new Date(flight.arrival).toLocaleString()}</td>
                        </tr>
                    )
                })}
            </tbody>
        )
    }
});

ReactDOM.render(<FlightBookingApp />, document.getElementById('content'));