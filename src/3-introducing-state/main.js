var Ticker = React.createClass({
    getInitialState: function () { // https://facebook.github.io/react/docs/component-specs.html#getinitialstate
        return {secondsElapsed: 0};
    },
    componentDidMount: function () { // https://facebook.github.io/react/docs/component-specs.html#mounting-componentdidmount
        this.interval = setInterval(this.tick, 1000); // Call 'tick' every second.
    },
    componentWillUnmount: function () { // https://facebook.github.io/react/docs/component-specs.html#unmounting-componentwillunmount
        clearInterval(this.interval); // Stop calling 'tick' every second.
    },
    tick: function () { // Our own tick function as called by 'this.interval'
        this.setState({secondsElapsed: this.state.secondsElapsed + 1});
    },
    render: function () {
        return (
            <div>
                <h1>Hello, {this.props.name}!</h1>
                <p>Page open {this.state.secondsElapsed} seconds.</p>
            </div>
        );
    }
});

ReactDOM.render(
    <Ticker name="Fred"/>,
    document.getElementById('content')
);